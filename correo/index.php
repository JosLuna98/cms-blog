<?php
    $mensaje="";
    if(isset($_POST["envio"])){
        include("php/envioCorreo.php");
	$email = new email("JosTecHQ","josluna1098@gmail.com","xqvhiaewamligfml");
	$email->agregar($_POST["email"],$_POST["nombre"]);
	if ($email->enviar('Comentario recibido!',$contenido_html)){
            $mensaje= 'Mensaje enviado';
	}else{
            $mensaje= 'El mensaje no se pudo enviar';
            $email->ErrorInfo;
        }
    }
?>
<!doctype html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link rel="stylesheet" href="css/custom.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Enviar mail</title>
        
        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css"/>

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="../css/font-awesome.min.css">
                
        <!-- my css -->
        <link type="text/css" rel="stylesheet" href="../css/post.css"/>

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="../css/style.css"/>
        
    </head>

<body> 
    <!-- Header -->
    <header id="header">
        <!-- Nav -->
        <div id="nav">
            <!-- Main Nav -->
            <div> <!-- id="nav-fixed" -->
                <div class="container">
                    <!-- logo -->
                    <div class="nav-logo">
                        <a href="../index.html" class="logo"><h1>JosTecHQ</h1></a>
                    </div>
                    <!-- /logo -->
                </div>
            </div>
            <!-- /Main Nav -->
        </div>
        <!-- /Nav -->
    </header>
    <!-- /Header -->
    
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div id="contenido" class="m2">
                    <header>
                        <div id="logo"><img src="../img/logo-php.png"/></div>
                    </header>
                    <div id="pagina">
                        <section>
                            <div>
                                <form id="enviar-mail" class="col s10 offset-s1" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="application/x-www-form-urlencoded" name="envio">
                                    <!--35.237.178.127-->
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="email" type="email" name="email" autofocus maxlength="50" size="20">
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="nombre" type="text" name="nombre" autofocus maxlength="50" size="20">
                                            <label for="nombre">Nombre</label>
                                        </div>
                                        <input name="envio" value="si" hidden="hidden">
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea id="mensaje" class="materialize-textarea"></textarea>
                                            <label for="mensaje">Mensaje</label>
                                        </div>
                                    </div>
                                    <div class="row" id="loaders">
                                        <img id="spinner" src="../img/spinner.gif" width="150">
                                    </div>
                                    <div class="row">
                                        <div class="col s6">
                                            <button id="enviar" class="btn waves-effect waves-light  pink darken-2" type="submit" name="action">Enviar
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                        <div class="col s6">
                                            <button id="resetBtn" class="btn waves-effect waves-light light-blue darken-2" type="submit">Reset
                                                <i class="material-icons right">delete</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/mail.js"></script>
</body>
</html>