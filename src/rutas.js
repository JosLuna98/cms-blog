import Backend from "./components/Backend.vue"
import Principal from "./components/Principal.vue"
import Login from "./components/Login.vue"
import Signin from "./components/Signin.vue"
import Perfil from "./components/Perfil.vue"

export const rutas = [
    { path:'', component: Principal },
    { path:'/backend', component: Backend },
    { path:'/login', component: Login },
    { path:'/signin', component: Signin },
    { path:'/profile', component: Perfil }
]